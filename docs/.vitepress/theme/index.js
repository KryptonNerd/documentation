import DefaultTheme from 'vitepress/theme'
import './custom.css'

import VueFeather from 'vue-feather'

import { watch } from 'vue'

export default {
  ...DefaultTheme,

  enhanceApp(ctx) {
    DefaultTheme.enhanceApp(ctx)

    ctx.app.component(VueFeather.name, VueFeather)

    setupMatomo(ctx.app, ctx.router)
      .catch(err => console.error(err))
  }
}


async function setupMatomo (app, router) {
  if (import.meta.env.SSR) return
  if (navigator.doNotTrack === 'yes' || navigator.doNotTrack === '1') return

  const VueMatomo = await import('vue-matomo')

  app.use(VueMatomo.default, {
    host: 'https://stats.framasoft.org/',
    siteId: 70,
    requireConsent: false,
    trackInitialView: false,
    trackerFileName: 'p',
    enableLinkTracking: true
  })

  watch(
    () => router.route.path,
    (path) => {
      console.log(router.route.data.title)
      console.log(router.route)

      const _paq = window._paq || []
      _paq.push([ 'setCustomUrl', path ])
      _paq.push([ 'setDocumentTitle', router.route.data.title ])
      _paq.push([ 'trackPageView' ])
    }
  )
}
