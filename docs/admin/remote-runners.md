# Remote Runners

::: info
This feature is available in PeerTube >= 5.2
:::

PeerTube can offload high CPU/long job like transcoding to a remote PeerTube runner.

When the remote runners option is enabled for a particular task, PeerTube stops to process the jobs locally and wait for a remote runner to request, accept and process them. The runner finally post the job result to PeerTube.

  * A more complete architecture documentation is available on [the Architecture documentation](/contribute/architecture#remote-vod-live-transcoding).
  * A detailed documentation to configure a runner is available on [CLI -> PeerTube runner](/maintain/tools#peertube-runner).

## Enable remote runners

The following tasks can be processed by remote runners, if enabled:
 * [VOD transcoding](/admin/configuration#vod-transcoding) in `Administration > Configuration > VOD transcoding`
 * Studio transcoding in `Administration > Configuration > VOD transcoding`
 * [Live transcoding](/admin/configuration#live-streaming) in `Administration > Configuration > Live streaming`


When remote runners are enabled

::: warning
Registered runners can alter video content. It's the reason why you must have confidence in their administrator.
:::

## Manage remote runners

When one of the remote runner options is enabled in the PeerTube instance configuration, a new administration menu entry is available to list registered runners: `Administration -> System -> Remote runners`.

![](/assets/remote-runners/list-remote-runners.png)

A runner needs a **Runner registration token** to register itself to your PeerTube instance.
To list available registration tokens, click on the **Runner registration tokens** button.

![](/assets/remote-runners/list-runner-registration-tokens.png)

You can generate multiple registration tokens and revoke them. Revoking a registration token will also unregister associated remote runners.

## Manage runner jobs

To list remote runner jobs, go on `Administration -> System -> Runner jobs`

![](/assets/remote-runners/list-remote-jobs.png)

You can see all remote jobs in the table. A job can be in the following state:
 * *Pending*: the job is waiting for a runner to be processed
 * *Processing*: the job is being processed by a remote runner
 * *Completed*: the job has been processed by a remote runner
 * *Failed*: the job failed several times, so PeerTube decided to fail it
 * *Waiting for parent job*: the job is waiting for another job to finish before being available

A pending job can be cancelled.


You can also see the job content. The public payload is sent to the remote runner, and the private payload is only used internally by the PeerTube instance.

![](/assets/remote-runners/remote-job-content.png)

