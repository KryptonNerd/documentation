# Configurations

## PeerTube configuration

PeerTube configuration is loaded with [node-config](https://www.npmjs.com/package/config). You can have multiple config files that will be selected due to a specific [file load order](https://github.com/lorenwest/node-config/wiki/Configuration-Files#file-load-order).

The configuration file is parsed during application start, which means that PeerTube has to be restarted for the changes to have effect.

You can find the configuration options documented in the [default.yaml](https://github.com/Chocobozzz/PeerTube/blob/develop/config/default.yaml).


## Environment variables

 * `NODE_ENV`: Specify server mode (`production`, `dev` or `test`) to choose the appropriate configuration
 * `NODE_CONFIG_DIR`: Specify PeerTube configuration directory
 * `NODE_APP_INSTANCE`: Specify application number. If set, PeerTube will use the chosen configuration app number (`production-1.yaml` for example)
 * `PT_INITIAL_ROOT_PASSWORD`: Set up an initial administrator password. It must be 6 characters or more
 * `FFMPEG_PATH` and `FFPROBE_PATH`: Use custom FFmpeg/FFprobe binaries
 * `HTTP_PROXY` and `HTTPS_PROXY`: Use proxy for HTTP requests
 * `YOUTUBE_DL_DOWNLOAD_BEARER_TOKEN`: Token to send in `Authorization` HTTP header when downloading latest youtube-dl binary
 * `USE_VIEWERS_FEDERATION_V2`: Federate views and viewers using the new federation protocol to improve performance (less messages are sent to followers). Introduced in PeerTube 6.1


## Security

Installing PeerTube following the production guide should be secure enough by default. We list here suggestions
to tighten the security of some parts of PeerTube.

#### Set up a HTTP proxy

With ActivityPub federation and import features, PeerTube does many HTTP requests to the external world.
To prevent private network/URL access, we encourage to use a HTTP proxy using `HTTP_PROXY` and `HTTPS_PROXY` environment variables.

#### Systemd Unit with reduced privileges

A systemd unit template is provided at `support/systemd/peertube.service`. Some directives can be changed to improve security!

 * `PrivateDevices=true`: sets up a new `/dev` mount for the Peertube process and
only adds API pseudo devices like `/dev/null`, `/dev/zero`, or `/dev/random`
but not physical devices. **This won't work on Raspberry Pi. That's why we don't enable it by default**

 * `ProtectHome=true`: sandboxes Peertube such that the service can not access the
 `/home`, `/root`, and `/run/user` folders. If your local Peertube user has its
  home folder in one of the restricted places, either change the home directory
  of the user or set this option to `false`


## Scalability

Here is some advice if you plan to manage a large PeerTube instance that may have many viewers or uploaders.

### Many concurrent viewers

If you plan to have many concurrent viewers (~ 1000) on a PeerTube video: we recommend to  and adapt some configuration keys:

 * Use the [recommended installation](/install/any-os) guide with nginx serving PeerTube public static files
 * Have at least 4 CPU cores and 4GB of RAM
 * Use the default PeerTube configuration (in your `production.yaml`)
 * Use `warn` log level in PeerTube configuration (`log.level`) to reduce log overhead or disable HTTP requests logging (`log.log_http_requests`)
 * Disable HTTP request duration metrics (`open_telemetry.metrics.http_request_duration.enabled`) if you enabled OpenTelemetry metrics
 * Use the V2 viewers federation protocol (`USE_VIEWERS_FEDERATION_V2=true` environment variable, introduced in PeerTube 6.1)
 * Disable all PeerTube plugins

If you plan to have more concurrent viewers, consider in addition to:

 * Disable client logs (`log.accept_client_log`)
 * Disable OpenTelemetry metrics (`open_telemetry.metrics.enabled`)
 * Help to distribute video static files using:
    * A CDN in front of PeerTube
    * [Object Storage](/maintain/remote-storage)
    * [PeerTube redundancy](/admin/following-instances#instances-redundancy)
 * Forbid access to `/api/v1/videos/{videoID}/views` and `/api/v1/metrics/playback` in your reverse proxy so PeerTube does not handle these API calls (you'll loose views and viewers statistics)

### Many videos

To handle many videos uploaded/imported on your instance, we recommend to:
 * Setup [Object Storage](/maintain/remote-storage) to store video files on a remote storage
 * Setup [Remote Runners](/admin/remote-runners) to offload transcoding jobs
