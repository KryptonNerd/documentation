# Video statistics

You can find statistics about your videos under **My library** -> **Videos** -> `...` video icon -> **Stats**.

All these statistics aggregate watch session coming from your PeerTube instance, remote PeerTube instances and from PeerTube embed integrated into external websites.

![](/assets/stats/video-stats.png)

You can also see a graph of views, watch time or countries (if your admin enabled this feature).

![](/assets/stats/video-graphs.png)
